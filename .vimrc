set linebreak
set relativenumber
set number
let mapleader = ','

set guioptions -=T
set guioptions -=m

set splitbelow
set splitright

"set mouse=a

filetype plugin on
set exrc


"<S-F1> to open relevant configuration files
nnoremap <S-F1> :<C-\>e"tabnew ~/.vim/ftplugin/".&filetype.".vim \| split ~/.vimrc\ \|vsplit .vimrc\| nnoremap <buffer> <S-F1> :tabclose".ReturnCR()<CR><CR>
nnoremap <C-F1> :<C-\>e"tabnew ~/.vim/UltiSnips/".&filetype.".snippets \| vsplit ~/.vim/plugged/vim-snippets/snippets/".&filetype.".snippets\|nnoremap <buffer> <C-F1> :tabclose".ReturnCR()<CR><CR>
nnoremap <S-F12> :mksession! session.vim<CR>
nnoremap <F12> :so session.vim<CR>
nnoremap <F2> :tabnew README.md<CR>
nnoremap <F3> :tabnew .gitlab-ci.yml<CR>
nnoremap <F4> :tabnew api.yml<CR>
nnoremap <C-F5> :copen<CR>
nnoremap <F5> :cn<CR>
nnoremap <S-F5> :cp<CR>
nnoremap <C-f> :vimgrep <C-r><C-w> ##<CR>

function ReturnCR()
	return "<CR>"
endfunction

tnoremap <C-PageDown> <C-w>gt
tnoremap <C-PageUp>   <C-w>gT
tnoremap <C-j>        <C-w>j
tnoremap <C-h>        <C-w>h
tnoremap <C-k>        <C-w>k
tnoremap <C-l>        <C-w>l
tnoremap ²            <C-w>:bn!<CR>
tnoremap ³            <C-w>:bp!<CR>

inoremap ù <Esc>
vnoremap ù <Esc>
cnoremap ù <Esc>
"ctrl s shortcut
nnoremap <C-s> :w<CR>
inoremap <C-s> <Esc>:w<CR>a


nnoremap <S-PageDown>  L<PageDown>
nnoremap <S-PageUp>    H<PageUp>

nnoremap gd <C-]>



vnoremap <C-s> :s/
nnoremap /     /
nnoremap ?     ?

nnoremap ³ :bp<CR>
nnoremap ² :bn<CR>

for i in range(80,83)
	let function_number = i - 79
	execute "map \e[1;2" . nr2char(i) . " <S-F". function_number . ">"
	execute "map \e[1;5" . nr2char(i) . " <C-F". function_number . ">"
endfor


"session options
set sessionoptions+=resize

"visual line up and down instead of raw line
nnoremap j gj
nnoremap k gk

nnoremap <M-c> "+yyyy
inoremap <M-c> "+yyyy
vnoremap <C-c> "+y

nnoremap <C-a> ^
nnoremap <M-a> d0
nnoremap <M-e> d$
nnoremap <C-e> $

nnoremap + <C-a>
nnoremap µ <C-x> 

inoremap <C-a> <Esc>^i
inoremap <C-<> <C-a>
inoremap <C-a> <Esc>^i
inoremap <M-a> <C-u>
inoremap <M-e> <Esc>d$a
inoremap <C-e> <Esc>$a
inoremap <C-u> <Esc>u

"the text between ' gets evaluated by the <expr> from the mapping and the text
"between " gets evaluated by <C-\>e
"first <CR> evaluates <C-\>e, second <CR> flushes the normal command
nnoremap <expr> - ':<C-\>e "m ".( ' . (-1 - v:count1) . ')<CR><CR>'
nnoremap <expr> _ ':<C-\>e "m +".( ' . (0 + v:count1) . ')<CR><CR>'
vnoremap <expr> - ':m '. (-1 - v:count1) .'<CR>'
vnoremap <expr> _ ':m +'. (0 + v:count1) .'<CR>'
imap --- <Esc>-a
imap ___ <Esc>_a


nnoremap $ g_


nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

command! Scratch new | setlocal bt=nofile bh=hide noswapfile

cnoremap $( ``<Left>

"easy omnifunc mappings
inoremap <C-x> <C-x><C-o>
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>"       : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>"       : "\<S-Tab>"
"I don't know why simple <C-y> does not work so let's just do <Esc>a
inoremap <expr> <cr>    pumvisible() ? "\<C-y><Esc>a" : "\<cr>"


function Unmapesc()
	iunmap ù
endfunction
function Conjunctions()
	inoreab <buffer>  otoh  on the other hand
	inoreab <buffer>  Otoh  On the other hand
	inoreab <buffer>  ntl   nevertheless
	inoreab <buffer>  Ntl   Nevertheless
	inoreab <buffer>  hv    however
	inoreab <buffer>  Hv    However
	inoreab <buffer>  d     despite
	inoreab <buffer>  D     Despite
	inoreab <buffer>  iso   in spite of
	inoreab <buffer>  Iso   In spite of
	inoreab <buffer>  fm    furthermore
	inoreab <buffer>  Fm    Furthermore
	inoreab <buffer>  t     the
	inoreab <buffer>  T     The
	inoreab <buffer>  h     here
	inoreab <buffer>  H     Here
	inoreab <buffer>  w     with
	inoreab <buffer>  wo    without
endfunction

function NoConjunctions()
	iuna <buffer> otoh
	iuna <buffer> Otoh
	iuna <buffer> ntl
	iuna <buffer> Ntl
	iuna <buffer> hv
	iuna <buffer> Hv
	iuna <buffer> d
	iuna <buffer> D
	iuna <buffer> iso
	iuna <buffer> Iso
	iuna <buffer> fm
	iuna <buffer> Fm
	iuna <buffer> t
	iuna <buffer> T
	iuna <buffer> h
	iuna <buffer> H
	iuna <buffer> w
	iuna <buffer> wo
endfunction

autocmd BufEnter,BufNewFile * call Conjunctions()

inoreab environ environment

function Security()
	inoreab <buffer> priv    private
	inoreab <buffer> pub     public
	inoreab <buffer> k       key
	inoreab <buffer> sym     symmetric
	inoreab <buffer> asym    asymmetric
	inoreab <buffer> symk    symmetric key
	inoreab <buffer> asymk   asymmetric key
	inoreab <buffer> crypto  cryptography
	inoreab <buffer> secu    security
	inoreab <buffer> CIA     Confidentiality, Integrity, Availability
	inoreab <buffer> cty     Confidentiality
	inoreab <buffer> Cty     Confidentiality
	inoreab <buffer> c       confidential
	inoreab <buffer> C       Confidential
	inoreab <buffer> ity     integrity
	inoreab <buffer> Ity     Integrity
	inoreab <buffer> avity   availability
	inoreab <buffer> Avity   Availability
endfunction

function NoSecurity()
	iuna <buffer> priv
	iuna <buffer> pub
	iuna <buffer> k
	iuna <buffer> sym
	iuna <buffer> asym
	iuna <buffer> symk
	iuna <buffer> asymk
	iuna <buffer> crypto
	iuna <buffer> secu
	iuna <buffer> CIA
	iuna <buffer> cty
	iuna <buffer> Cty
	iuna <buffer> c
	iuna <buffer> C
	iuna <buffer> ity
	iuna <buffer> Ity
	iuna <buffer> avity
	iuna <buffer> Avity
endfunction

"more 

"I don't know why I have to do this so that alt keys work ???
for i in range(97,122)
  let c = nr2char(i)
  exec "map  \e".c." <M-".c.">"
  exec "map! \e".c." <M-".c.">"
endfor


call plug#begin()

Plug 'aserebryakov/vim-todo-lists'
Plug 'preservim/vim-markdown'
Plug 'xolox/vim-misc'
Plug 'lervag/vimtex'
Plug 'morhetz/gruvbox'
Plug 'valloric/youcompleteme'
Plug 'altercation/vim-colors-solarized'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'preservim/tagbar'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-surround'
"Plug 'scrooloose/syntastic'
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'
"Plug 'metakirby5/codi.vim'
Plug 'raimondi/delimitmate'
Plug 'mustache/vim-mustache-handlebars'
Plug 'w0rp/ale'
Plug 'godlygeek/tabular'
Plug 'xolox/vim-notes'
Plug 'felipec/vim-sanegx'
Plug 'dart-lang/dart-vim-plugin'

Plug 'weirongxu/plantuml-previewer.vim'
Plug 'tyru/open-browser.vim'
Plug 'aklt/plantuml-syntax'
Plug 'vim-scripts/DrawIt'
Plug 'rust-lang/rust.vim'
call plug#end()

colorscheme gruvbox
set bg=dark

"NERDTree stuff
" Start NERDTree, unless a file or session is specified, eg. vim -S session_file.vim.
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists('s:std_in') && v:this_session == '' | NERDTree | endif

" Start NERDTree. If a file is specified, move the cursor to its window.
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif

" Start NERDTree and put the cursor back in the other window.
"autocmd VimEnter * NERDTree | wincmd p

" Start NERDTree when Vim is started without file arguments.
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif 

" Exit Vim if NERDTree is the only window remaining in the only tab.
"autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Close the tab if NERDTree is the only window remaining in it.
"autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif 

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
"autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
												"\ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif 

" Open the existing NERDTree on each new tab.
" Note: this does not work when using copen
"autocmd BufEnter * if getcmdwintype() == '' | silent NERDTreeMirror | endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"vim gitgutter plugin stuff

command! Gqf GitGutterQuickFix | copen
set updatetime=100

"youcomplete plugin stuff

set completeopt-=preview

"ulti-snips stuff


let g:UltiSnipsExpandTrigger="<C-q>"

"you complete me stop because it fucks up codi

"toggle ycm
"originally <M-y> but pressing ù did the same thing for some reason  
nnoremap <M-t> :let b:ycm_largefile=!b:ycm_largefile<CR>

" bufferline stuff

let g:bufferline_echo = 0
