# My Vimrc plus zsh configuration

# Installation

You of course need to have vim and zsh installed and oh-my-zsh installed

plugins need to be installed using vim-plug i.e. entering :PlugInstall in vim

Some of those plugins need additionnal input for them to work such as ycm. This is not included in this repository.

# Security

Some stuff should be put on a public git. All files beginning with the word private are ignored
