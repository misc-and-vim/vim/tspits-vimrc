#!/usr/bin/bash

sed -ri "s/tspits/$(whoami)/g" .zshrc

ln -s $(pwd)/ftplugin/ 	$HOME/.vim
ln -s $(pwd)/UltiSnips/ $HOME/.vim
ln -s $(pwd)/.p10k.zsh 	$HOME/.p10k.zsh
ln -s $(pwd)/custom	$HOME/.oh-my-zsh
ln -s $(pwd)/.vimrc 	$HOME/.vimrc
ln -s $(pwd)/.zshrc 	$HOME/.zshrc
