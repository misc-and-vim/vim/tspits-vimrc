setopt RC_QUOTES

#
SOCK_DIR=/tmp/dtach_sockets

if [[ -d $SOCK_DIR ]]; then
else
	mkdir $SOCK_DIR
fi

alias da='f() { dtach -a $1 }; f'
alias dn='f() { dtach -n '$SOCK_DIR/'$1 $2 $3 $4 $5 $6 $7}; f'
alias dq='for file in $(ls '$SOCK_DIR/'); do rm '$SOCK_DIR/'$file; done'
alias dp='dtach -p'
_dtach() {
	local sockets=($(ls $SOCK_DIR))
	sockets=$(echo $SOCK_DIR/$^sockets)
	if [[ $sockets == $SOCK_DIR/ ]]; then
		sockets=""
	fi
	_arguments "1:sockets:($sockets)" "2:command:(vim)"
}

setopt complete_aliases
compdef _dtach da dc dp
