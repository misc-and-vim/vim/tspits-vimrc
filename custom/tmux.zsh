alias t="tmux"
alias tl="t ls"
alias ta='t attach -t'
alias tn='f() {t new-session -ds "$1"};f'
alias tk='t kill-session -t'
alias tng='t new-session'

_tl() {
	tmux ls &>/dev/null
	if [[ $? -eq 0 ]] 
	then
		local -a sessions
		sessions=($(tmux ls | egrep -o '^[a-zA-Z0-9]+'))
		_alternative "sessions:sessions:($sessions)"
	fi
}

setopt complete_aliases
compdef _tl ta tk

