

alias L="less"
alias M="more"
alias H="head"
alias T="tail"

setopt rcquotes
#R to set tunnel back to here to open files on client
alias ss="ssh -R20202:localhost:22 tspits@172.105.248.197"
alias ssl="ssh -p20202 tanguyspits@localhost"

autoload -Uz read-from-minibuffer

copy-command-to-clipboard () {
	echo -n $BUFFER | xclip -selection clipboard
}

zle -N copy-command-to-clipboard  
bindkey '^[c' copy-command-to-clipboard  

prepend-sudo () {
	if [[ $BUFFER != "sudo "* ]]; then
		BUFFER="sudo $BUFFER"
	fi
}

zle -N prepend-sudo
bindkey '^q' prepend-sudo

prepend-echo () {
	BUFFER="echo $BUFFER"
}

zle -N prepend-echo
bindkey '^[q' prepend-echo

redirect-to-null () {
	BUFFER="$BUFFER &>/dev/null"
}

zle -N redirect-to-null
bindkey '^[r' redirect-to-null

delete-left () {
	LBUFFER=""
}

zle -N delete-left
bindkey '^[a' delete-left

delete-right () {
	RBUFFER=""
}

zle -N delete-right
bindkey '^[e' delete-right

alias tfe="cd ~/Documents/ulg/TFE"
