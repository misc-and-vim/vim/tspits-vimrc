setlocal ts=2
setlocal sts=2
setlocal sw=2

"Need this for tags with s:
setlocal iskeyword+=:

nnoremap <buffer> <F1> :w\|so%<CR>

vnoremap <buffer> <F1> :Tabularize/\v\s+/l0<CR>

"open plugin github page while cursor is on a plugin name
nnoremap <buffer> <F2> :!firefox https://github.com/<C-r><C-a><CR>
