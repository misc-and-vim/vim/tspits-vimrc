setlocal foldmethod=indent
setlocal sw=2
setlocal sts=2
setlocal ts=2

nnoremap <F1> :%!jq '.'<Left> 
vnoremap <F1> !jq '.'<Left>
