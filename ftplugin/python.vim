setlocal expandtab
setlocal sw=4
setlocal ts=4
setlocal sts=4
let b:ale_fixers = {
			\	'python':[
			\		'autoimport',
			\		'autopep8',
			\		],
			\	}

nnoremap <S-F2> :bo term ++close python -i %<CR>
nnoremap <C-F2> :bo term ++close poetry run python -i %<CR>

au BufWritePre <buffer> ALEFix
"au BufWritePost <buffer> !poetry run pytest
